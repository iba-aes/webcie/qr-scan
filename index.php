<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Scan Tickets</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
</head>
<body>
<style>
    canvas {
        display: none;
    }
    hr {
        margin-top: 32px;
    }
    input[type="file"] {
        display: block;
        margin-bottom: 16px;
    }
    div {
        margin-bottom: 16px;
    }

    .fixednotice{
        top:0;
        background: yellow;
        padding: 10px;
        color: black;
    }

    table{
        background-color: white;
    }
</style>
<div class="fixednotice"><span id="gaan" style="font-size: 60px;">Klaar om te scannen...</span></div>

<form action="#">
    <input type="text" id="code-text">
    <input type="submit" value="Scan deze code!" id="submit-code">
</form>

<div>
    <div class="col-sm-12"><h3>Tickets: Diesgala: Casino Royale</h3><h5>13 februari 2024, 21:00 - 02:00</h5><video muted playsinline id="qr-video" width="100%"></video></div>
</div>
<hr>
<div class="col-sm-12">
<table class="table table-striped table-bordered">
    <tr>
        <td>Scancontrole-URL</td>
        <td><span id="scancontrole-url">-</span></td>
    </tr>
    <tr>
        <td>QR Waarde</td>
        <td><span id="cam-qr-result">-</span></td>
    </tr>
    <tr>
        <td>Laatste scan</td>
        <td><span id="cam-qr-result-timestamp"></span></td>
    </tr>
    <tr>
        <td>CameraOn</td>
        <td><span id="cam-has-camera"></span></td>
    </tr>
</table>
</div>



<script type="module">
    import QrScanner from "./qr-scanner.min.js";
    QrScanner.WORKER_PATH = './qr-scanner-worker.min.js';

    /* Vul hier de scancontrole-URL van de activiteit in
     * (bijvoorbeeld 'https://www.a-eskwadraat.nl/Activiteiten/bmn/7693/BtaMusicNight2019/ScanControle'
     */
    const scanControleURL = 'https://www.a-eskwadraat.nl/Activiteiten/dies2425/9628/DiesgalaCasinoRoyale/ScanControle';

    const scanControleURL_display = document.getElementById('scancontrole-url');
    const video = document.getElementById('qr-video');
    const camHasCamera = document.getElementById('cam-has-camera');
    const camQrResult = document.getElementById('cam-qr-result');
    const camQrResultTimestamp = document.getElementById('cam-qr-result-timestamp');
    const fileSelector = document.getElementById('file-selector');
    const fileQrResult = document.getElementById('file-qr-result');
    var magnogeenkeer = true;

    scanControleURL_display.innerHTML = scanControleURL;

    // Gebruik een formpje voor debuggen, of als men de code heeft maar niet als QR
    const submitCode = document.getElementById('submit-code');
    const codeText = document.getElementById('code-text');
    submitCode.onclick = function() {
        var text = codeText.value;
        console.log(text);
        setResult(camQrResult, text);
    }

    function setResult(label, result) {

        
        label.textContent = result;
        camQrResultTimestamp.textContent = new Date().toString();
        label.style.color = 'teal';
        clearTimeout(label.highlightTimeout);
        label.highlightTimeout = setTimeout(() => label.style.color = 'inherit', 100);

        if(magnogeenkeer){
            magnogeenkeer = false;
            $.ajax({
                type: 'GET',
                url: scanControleURL + '?code=' + result,
                dataType: 'json',
                crossDomain: true,
                success: function(data)
                {
                  console.log(data);
                  if (data.valid) {
                    $('body').css('background-color', data.color);
                    $('body').css('color','white');
                    $('#gaan').html(data.error);
                    var audio = new Audio('ok.mp3');
                    audio.play();
                    setTimeout(function(){ $('body').css('background-color','white'); }, 3500);
                    setTimeout(function(){ $('body').css('color','black'); }, 3500);
                    setTimeout(function(){ $('#gaan').text('Klaar om te scannen...'); }, 3500);
                    setTimeout(function(){ magnogeenkeer = true; }, 3500);
                  } else {
                    $('body').css('background-color', data.color);
                    $('body').css('color','white');
                    $('#gaan').html(data.error);
                    setTimeout(function(){ $('body').css('background-color','white'); }, 3500);
                    setTimeout(function(){ $('body').css('color','black'); }, 3500);
                    setTimeout(function(){ $('#gaan').text('Klaar om te scannen...'); }, 3500);
                    setTimeout(function(){ magnogeenkeer = true; }, 3500);
                  }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('body').css('background-color','pink');
                    $('body').css('color','black');
                    $('#gaan').html(textStatus + errorThrown);
                    setTimeout(function(){ $('body').css('background-color','white'); }, 3500);
                    setTimeout(function(){ $('body').css('color','black'); }, 3500);
                    setTimeout(function(){ $('#gaan').text('Klaar om te scannen...'); }, 3500);
                    setTimeout(function(){ magnogeenkeer = true; }, 3500);
                },
                xhrFields: { withCredentials:true }
              });
        }

    }

    // ####### Web Cam Scanning #######

    QrScanner.hasCamera().then(function (hasCamera) {
        camHasCamera.textContent = hasCamera;

        if (!hasCamera) return;

        const scanner = new QrScanner(video, result => setResult(camQrResult, result));
        scanner.start();

        document.getElementById('inversion-mode-select').addEventListener('change', event => {
            scanner.setInversionMode(event.target.value);
        });
    });

    // ####### File Scanning #######
    /*

    fileSelector.addEventListener('change', event => {
        const file = fileSelector.files[0];
        if (!file) {
            return;
        }
        QrScanner.scanImage(file)
            .then(result => setResult(fileQrResult, result))
            .catch(e => setResult(fileQrResult, e || 'No QR code found.'));
    });
    */

</script>
</body>
</html>
